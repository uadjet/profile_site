// d3.select("#email").text("LustyanM@gmail.com");

function on() {
  document.getElementById("overlay").style.display = "block";
  d3.select("#text")
    .text("LustyanM@gmail.com")
    .attr("onclick", "copyToClipboard()")
    .style("z-index", "5")
    .append("p")
    .text("click to copy")
    .attr("class", "f6");
  // .attr("class", "btn")
  // .attr("data-clipboard-text", "LustyanM@gmail.com");
}

function off() {
  document.getElementById("overlay").style.display = "none";
}

function copyToClipboard() {
  // Create an auxiliary hidden input
  var aux = document.createElement("input");

  // Get the text from the element passed into the input
  aux.setAttribute("value", "LustyanM@gmail.com");

  // Append the aux input to the body
  document.body.appendChild(aux);

  // Highlight the content
  aux.select();

  // Execute the copy command
  document.execCommand("copy");

  // Remove the input from the body
  document.body.removeChild(aux);
  // Borrowed with love from https://jsfiddle.net/alvaroAV/a2pt16yq/
}
