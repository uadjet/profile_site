let w = document.documentElement.clientWidth.toString();
let h = document.documentElement.clientHeight.toString();

let d = Math.floor(Math.sqrt(Math.pow(w, 2) + Math.pow(h, 2)));

function rnd_norm() {
  return (
    (Math.random() +
      Math.random() +
      Math.random() +
      Math.random() +
      Math.random() +
      Math.random() +
      Math.random() +
      Math.random() +
      Math.random() +
      Math.random() -
      5) /
    5
  );
}

ws = Array.from({ length: 400 }, () => rnd_norm() * w * 0.91 + w / 2);
hs = Array.from({ length: 400 }, () => rnd_norm() * h * 0.91 + h / 2);

let data = ws.map(function(e, i) {
  return [e, hs[i]];
});

let delaunay = new d3.Delaunay.from(data);

const ssg = d3
  .select("#container")
  .append("svg")
  .attr("width", "100%")
  .attr("height", "100%")
  .attr("display", "block")
  .attr("margin", "auto")
  .attr("id", "ssg")
  // .style("position", "-webkit-sticky")
  .style("position", "absolute")
  .style("z-index", "-9");

ssg
  .append("rect")
  .attr("width", w * 1)
  .attr("height", h * 1)
  .style("fill", "none");

ssg
  .append("path")
  .attr("fill", "none")
  .attr("stroke", "#96ccff")
  .attr("d", delaunay.render())
  .attr("opacity", "0.0");

function repeat_path() {
  d3.select("path")
    .transition()
    .duration(7500)
    .attr("opacity", "0.25")
    .transition()
    .duration(7500)
    .attr("opacity", "0.15")
    .on("end", repeat_path);
}

repeat_path();
